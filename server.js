const express = require('express')
const app = express();
const formidable = require('formidable');
const Promise    = require('bluebird');
const fs         = require('fs');
const path       = require('path')

Promise.promisifyAll(fs);

var imageDirectory = './img';
var domain = 'blazeit.solutions'
var secret = 'x'

app.use(express.static('img'))

app.get('/', function(req, res) { 
    res.redirect('https://www.youtube.com/playlist?list=PLk2PflOQHdsjwhQ5Gbg2BaX4GSwZLycAK');
})

function generateFileName(type) {
    var name = Math.random().toString(36).substring(2, 7);
    return Promise.try(function() {
        return fs.openAsync(imageDirectory + '/' + name + type, 'r');
    })
    .then(function(fd) {
        return fs.close(fd);
    })
    .then(function() {
        return generateFileName(type);
    })
    .catch(function(err) {
        if (err.code != 'ENOENT')
            throw err;
        return name + type;
    })
}

app.post('/upload', function(req, res) {
    var form = new formidable.IncomingForm();
    form.keepExtensions = true;
    form.maxFieldsSize = 128 * 1024 * 1024; //128 mb.
    form.uploadDir = './temp';
    form.parseAsync = Promise.promisify(form.parse, {multiArgs: true});
    Promise.promisifyAll(form);

    Promise.try(function() {
        return form.parseAsync(req);
    })
    .spread(function(fields, file) {
        if (fields.secret != secret) {
            res.end('Invalid secret');
            return;
        }
        var file = file.file;
        var newFileName;
        return Promise.try(function() {
            var extension = path.extname(file.path);
            return generateFileName(extension);
        })
        .then(function(newName) {
            newFileName = newName;
            var newFilePath = imageDirectory + '/' + newFileName;
            console.dir(newFilePath)
            return fs.renameAsync(file.path, newFilePath);
        })
        .then(function() {
            console.log('File uploaded succesfully: ' + newFileName);
            res.json({
                status: 200,
                data: {
                    "link": 'http://' + domain + '/' + newFileName
                }
            })
        })
    })
})

app.listen(14201, () => console.log('File uploader listening on port 14201!'))